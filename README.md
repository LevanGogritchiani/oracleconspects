 BEGIN
    --
    for dep in c_dep_info loop
      --
      v_dep_rab(dep.department_id) := t_dep_info(old_salary => dep.salary_sum,
                                                 new_salary => dep.salary_sum);
      --
    end loop;
    --
  END BEFORE STATEMENT;

  AFTER EACH ROW IS
  BEGIN
    --
    if nvl(:new.salary, 0) <> nvl(:old.salary, 0) or
       nvl(:new.department_id, 0) <> nvl(:old.department_id, 0) then
      --
      if :old.department_id is not null then
        v_dep_rab(:old.department_id).new_salary := v_dep_rab(:old.department_id).new_salary -
                                                     :old.salary;
      end if;
      if :new.department_id is not null then
        v_dep_rab(:new.department_id).new_salary := v_dep_rab(:new.department_id).new_salary +
                                                     :new.salary;
      end if;
      --
    end if;
    --
  END AFTER EACH ROW;
  ------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE TRIGGER compound_trigger_example
FOR INSERT OR UPDATE OR DELETE ON your_table
COMPOUND TRIGGER

  -- Declare variables for statement level actions
  statement_data VARCHAR2(100);

  BEFORE STATEMENT IS
  BEGIN
    null;
  END BEFORE STATEMENT;

  BEFORE EACH ROW IS
  BEGIN
    -- Row level actions
    IF INSERTING THEN
      DBMS_OUTPUT.PUT_LINE('Inserting row with ID: ' || :NEW.id);
    ELSIF UPDATING THEN
      DBMS_OUTPUT.PUT_LINE('Updating row with ID: ' || :OLD.id || ' to ' || :NEW.id);
    ELSIF DELETING THEN
      DBMS_OUTPUT.PUT_LINE('Deleting row with ID: ' || :OLD.id);
    END IF;
  END BEFORE EACH ROW;

  AFTER EACH ROW IS
  BEGIN
    null;
  END AFTER EACH ROW;

  AFTER STATEMENT IS
  BEGIN
   null;
  END AFTER STATEMENT;

END compound_trigger_example;
/
///////////////////////////////////////////////////////////////////////////////

--N1--
შექმენით ტრიგერი EMPLOYEES ცხრილისთვის, რომელიც Employees ცხრილი ყოველ ცვლილებას დალოგავს ცალკე ცხრილში. 
ამ ახალ ცხრილს ქონდეს Employees ცხრილის ყველა ველი, დამატებით LOG_TIME, LOG_USER, LOG_EVENT (CREATE, UPDATE, DELETE).

create table EMPLOYEES_LOG
(
  LOG_ID         NUMBER generated by default as identity,
  employee_id    NUMBER(6) not null,
  first_name     VARCHAR2(20),
  last_name      VARCHAR2(25) not null,
  email          VARCHAR2(25) not null,
  phone_number   VARCHAR2(20),
  hire_date      DATE not null,
  job_id         VARCHAR2(10) not null,
  salary         NUMBER(12,2),
  commission_pct NUMBER(2,2),
  manager_id     NUMBER(6),
  department_id  NUMBER(4),
  LOG_TIME       DATE default sysdate,
  LOG_USER       varchar2(50) default user,
  LOG_EVENT      VARCHAR2(25)
);

create or replace trigger EMPLOYEES_AIUD_TRG
  after insert or update or delete on employees
  for each row
declare
  v_logging_event varchar2(25);
begin
  v_logging_event := case
                       when updating then
                        'UPDATE'
                       when inserting then
                        'INSERT'
                       when deleting then
                        'DELETE'
                     end;
  --
  if inserting then
    --
    insert into employees_log
      (employee_id,
       first_name,
       last_name,
       email,
       phone_number,
       hire_date,
       job_id,
       salary,
       commission_pct,
       manager_id,
       department_id,
       log_event)
    values
      (:new.employee_id,
       :new.first_name,
       :new.last_name,
       :new.email,
       :new.phone_number,
       :new.hire_date,
       :new.job_id,
       :new.salary,
       :new.commission_pct,
       :new.manager_id,
       :new.department_id,
       v_logging_event);
    --
  else
    --
    insert into employees_log
      (employee_id,
       first_name,
       last_name,
       email,
       phone_number,
       hire_date,
       job_id,
       salary,
       commission_pct,
       manager_id,
       department_id,
       log_event)
    values
      (:old.employee_id,
       :old.first_name,
       :old.last_name,
       :old.email,
       :old.phone_number,
       :old.hire_date,
       :old.job_id,
       :old.salary,
       :old.commission_pct,
       :old.manager_id,
       :old.department_id,
       v_logging_event);
    --
  end if;
  --
end EMPLOYEES_AIUD_TRG;


--N2--
შექმენით ტრიგერი, რომელიც ახალ ცხრილში (თქვენ უნდა შექმნათ) შეინახავს თანამშრომლების ცხრილში ჩანაწერის ჩავარდნის თარიღს და თანამშრომელზე ბოლო ცვლილების თარიღს, ანუ ამ ცხრილში თითო თანამშრომელზეე მხოლოდ ერთი ჩანაწერი უნდა იყოს, input date insert -ის დროს დაისეტოს ხოლო last change date  ყოველი ცვლილების შემდეგ.

create table EMPLOYEES_CHANGE_LOG
(
  LOG_ID           NUMBER generated by default as identity,
  employee_id      NUMBER(6) not null,
  INPUT_DATE       DATE,
  LAST_UPDATE_DATE DATE
);

create or replace trigger EMPLOYEES_AIU_TRG
  after insert or update on employees
  for each row
begin
  if inserting then
    insert into employees_change_log
      (employee_id, input_date, last_update_date)
    values
      (:new.employee_id, sysdate, sysdate);
  elsif updating then
    --
    update employees_change_log
       set last_update_date = sysdate
     where employee_id = :new.employee_id;
    --
    if sql%notfound then
      --
      insert into employees_change_log
        (employee_id, input_date, last_update_date)
      values
        (:new.employee_id, sysdate, sysdate);
      --
    end if;
    --
  end if;
  --
end EMPLOYEES_AIU_TRG;

--N3--
შექმენით ტრიგერი EMPLOYEES ცხრილისთვის, რომელიც Employees ცხრილში JOB_ID შეცვლის შემთხვევაში, შესაბამის ჩანაწერს გააკეთებს JOB_HISTORY ცხრილში. ასევე დაააფდეითებს HIRE_DATE.

create or replace trigger EMPLOYEES_JOB_HIST_BUD_TRG
  before update or delete on employees
  for each row
begin
  --
  :new.hire_date := trunc(sysdate);
  --
  insert into job_history
    (employee_id, start_date, end_date, job_id, department_id)
  values
    (:old.employee_id, :old.hire_date, trunc(sysdate), :old.job_id, :old.department_id);
  --
end EMPLOYEES_JOB_HIST_BUD_TRG;


--N4--
შექმენით ცხრილი რომელსაც ექნება ველები: მინიმალური ხელფასი, მაქსიმალური ხელფასი. EMPLOYEES ცხრილში ყოველი ცვლილების შემდეგ დააფდეითეთ ეს ცხრილი up-to-date ინფორმაციით.

create table EMPLOYEE_INFO (
  min_salary number,
  max_salary number
);

create or replace trigger EMPLOYEES_INFO_AIUD_TRG
  before update or delete on employees
begin
  --
  delete from EMPLOYEE_INFO;
  --
  insert into EMPLOYEE_INFO
  select min(salary), max(salary) from employees;
  --
end EMPLOYEES_INFO_AIUD_TRG;
